

const success = {
    "estado": {
        "codigoEstado": "200",
        "glosaEstado": "OK"
    },
    "result": {
        "dosis1": {
            "centro": "Centro de Salud Familiar Dr. Norman Voulliéme, Cerrillos Region Metropolitana de Santiago",
            "vacuna": "CoronaVac",
            "laboratorio": "SINOVAC LIFE SCIENCE",
            "fecha": "31/03/2021",
            "lote": "G202102003"
        },
        "dosis2": {
            "centro": "Centro de Salud Familiar Dr. Norman Voulliéme, Cerrillos Region Metropolitana de Santiago",
            "vacuna": "CoronaVac",
            "laboratorio": "SINOVAC LIFE SCIENCE",
            "fecha": "28/04/2021",
            "lote": "L202103007"
        }


    }
};

module.exports = success;



