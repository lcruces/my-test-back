const {Router} = require('express');
const router = Router();
const success = require('../data/success');
const failure = require('../data/failure');



router.get('/userDetail/:rut', (request, response) =>{
    
    let respuesta= {};
    let rut = request.params.rut;

    switch (rut) {
        case '174911290':

            respuesta = failure;
           
          break;
        case '178364014':

            respuesta = success;
          
          break;
        case '123369564':
            respuesta = failure;
          break;
        case '102234871':
            respuesta = success;
          break;
        default:
            respuesta = success;
         
      }


    response.json(respuesta);

});

module.exports = router;

