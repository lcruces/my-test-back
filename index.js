const express = require('express');
const app = express();
const request = require('request');
const async = require('async');


app.use(require('./routes/index'));

app.listen('8010', () => {

    console.log('Listening on port 8010');
});